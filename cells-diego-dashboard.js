{
  const {
    html,
  } = Polymer;
  /**
    `<cells-diego-dashboard>` Description.

    Example:

    ```html
    <cells-diego-dashboard></cells-diego-dashboard>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-diego-dashboard | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsDiegoDashboard extends Polymer.Element {

    static get is() {
      return 'cells-diego-dashboard';
    }

    static get properties() {
      return {

        data: {
          type: String,
            value: "",
            observer: 'chuckQuote'
        },

            datostodos: {
            type: Object,
            value: [ 

            {
            img: '../images/dragon.jpeg',
            name: 'Oscar',
            last: 'Gonzalez',
            address: 'Lomas Estrella',
            hobbies: ['Videojuegos', 'Futbol', 'Dormir']
          },
          {
            img: 'http://placehold.it/150x150/000000/FFFFFF',
            name: 'Luis',
            last: 'Kelly Díaz',
            address: 'Una casa',
            hobbies: 'Programar'
          },
          {
            img: '../images/tresmonos.jpeg',
            name: 'Jesús',
            last: 'Guzmán Vitte',
            address: 'Calle Rancho La Laguna 46B, Colonia Fraccionamiento San Antonio, Cuautitlán Izcalli, Estado de México',
            hobbies: 'Wu-Shu'
          },
          {
            img: '../images/gato-agosto.jpg',
            name: 'Iris',
            last: 'Lizeth',
            address: 'Puerto Yavaros',
            hobbies: ['Ver peliculas y series', 'salir con amigos y familiares']
          },
          {
            img: '../images/migato.jpg',
            name: 'Jhony Fernando',
            last: 'Bartolo Diaz',
            address: 'Tlalpan Centro, Calle Francinso I Madero',
            hobbies: 'Salir de viaje'
          },
          {
            img: '../images/favicon.ico',
            name: 'Silvino',
            last: 'Piza Gaspar',
            address: 'Zumpango, Estado de México',
            hobbies: ['GYM', 'DIBUJAR', 'ESCUCHAR MÚSICA', 'JUGAR VIDEOJUEGOS', 'SALIR AL CINE']
          },
          {
            img: '../images/avatar.jpg',
            name: 'Victor Manuel',
            last: 'Roman Orozco',
            address: 'sur 119 Iztcalco, CDMX',
            hobbies: 'Futbol'
          },
          {
            img: '../images/icon-72x72.png',
            name: 'Ernesto',
            last: 'Mejia Camacho',
            address: 'calzada de la primera #163. tlahuac',
            hobbies: ['tocar la guitarra', 'jugar video juegos', 'hacer deporte']
          },
          {
            img: '../images/leon.jpeg',
            name: 'Luis Enrique',
            last: 'Ruiz Ruiz',
            address: 'Calle Adios #274',
            hobbies: ['Leer', 'jugar video juegos']
          },
          {
            img: '../images/cara.jpg',
            name: 'Ruben',
            last: 'de la Cruz',
            address: 'Calle 4 tetelpan',
            hobbies: ['Series', 'Musica', 'Jugar videjuegos']
          },
          {
            img: '../images/imagen1.jpeg',
            name: 'Diego',
            last: 'Vazquez Alvarez',
            address: 'av. jazmin 32 a',
            hobbies: 'ir al Cine'
          },
          {
            img: '../images/tony.jpg',
            name: 'Tony',
            last: '',
            address: '',
            hobbies: ['Programar', 'Netflix']
          },
          {
            img: '../images/mon.png',
            name: 'Monica',
            last: 'Rivera Valle',
            address: 'Chimalhuacán, Estado de México',
            hobbies: 'Ver series, películas'
          },
          {
            img: '../images/PkVweyp.jpg',
            name: 'Martin',
            last: 'Juarez',
            address: 'Tlaltenco Tlahuac',
            hobbies: ['leer', 'deportes', 'musica']
          },
          {
            img: '../images/favicon.ico',
            name: 'Alberto',
            last: '',
            address: 'Santiago',
            hobbies: ''
          },
          {
            img: '',
            name: 'memo',
            last: 'Ramirez',
            address: 'toledo col juarez',
            hobbies: ''
          }

            ]
          }
        };
    }
      filtrar(string) {
        if (!string) {
          return null;
        } else {
          string = string.toLowerCase();
          return function(datostodos) {
            var name = datostodos.name.toLowerCase();
            var last = datostodos.last.toLowerCase();
            return (name.indexOf(string) != -1 ||  last.indexOf(string) != -1);
          };
        }
      }


    static get template() {
      return html `
      <style include="cells-diego-dashboard-styles cells-diego-dashboard-shared-styles"></style>
      <slot></slot>

      
           <div class="card">
            <h2>Buscador</h2>
            <input type="text" placeholder="Filtro" value="{{buscarCadena::input}}"><br><br>

                <b> Chuck </b><br />
                <img src="{{data.icon_url}}"><br />
                [[data.value]] 

                <br>

               <template is="dom-repeat" items="{{datostodos}}" 
                filter="{{filtrar(buscarCadena)}}">

                    <div>
                        <img src="{{item.img}}">
                        <p>Nombre: <span>{{item.name}}</span></p>
                        <p>Apellido: <span>{{item.last}}</span></p> 
                        <p>Dirección: <span>{{item.address}}</span></p> 
                        <p>Hobbies: <span>{{item.hobbies}}</span></p>
                    </div>  
                </template>
        </div>
  
        <iron-ajax 
                auto 
                url="https://api.chucknorris.io/jokes/random" 
                handle-as="json" 
                last-response="{{data}}"> 
        </iron-ajax> 
      `;
    }
  }

  customElements.define(CellsDiegoDashboard.is, CellsDiegoDashboard);
}